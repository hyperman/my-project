<?php

require_once __DIR__. "/Autoload.php";

session_start();

require_once __DIR__."/dbconnect.php";
//Объявляем ячейку для добавления ошибок, которые могут возникнуть при обработке формы.
$_SESSION["error_messages"] = '';

//Объявляем ячейку для добавления успешных сообщений
$_SESSION["success_messages"] = '';

if(isset($_POST["btn_submit_auth"]) && !empty($_POST["btn_submit_auth"])) {

        if (!empty($_POST["username"]) && !empty($_POST["password"])) {
            $username = trim($_POST["username"]);
            $password = trim($_POST["password"]);

            if(!empty($username)){
                $username=htmlspecialchars($_POST['username']);
            } else {
                $_SESSION["error_messages"] .= "<p class='mesage_error' >Отсутствует поле для ввода Username</p>";
                //Возвращаем пользователя на страницу регистрации
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$address_site."/form_auth.php");
            }

            if(!empty($password)){
                $password = htmlspecialchars($password, ENT_QUOTES);
                //Шифруем пароль
                $password = md5($password."top_secret");
            }else{
                // Сохраняем в сессию сообщение об ошибке.
                $_SESSION["error_messages"] .= "<p class='mesage_error' >Укажите Ваш пароль</p>";

                //Возвращаем пользователя на страницу регистрации
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$address_site."/form_auth.php");
                //Останавливаем скрипт
                exit();
            }

        }
    $query_test = "SELECT id FROM `users` WHERE username = '".$username."' AND password = '".$password."'";
    $test_user = $pdo_driver->getConnection()->query($query_test);
    $batva = $test_user->fetch(PDO::FETCH_ASSOC);
    if(!$batva){
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error' >Ошибка запроса на выборке пользователя из БД</p>";

        //Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$address_site."/form_auth.php");

        //Останавливаем скрипт
        exit();
    } else {
        if(isset($batva['id'])){
            $_SESSION['username'] = $username;
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$address_site."/admin.php");
                // $header_into = file_get_contents("intopage.php");
                // echo $header_into;
        } else {
            $_SESSION["error_messages"] .= "<p class='mesage_error' >Неправильный логин и/или пароль</p>";

            //Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".$address_site."/form_auth.php");

            //Останавливаем скрипт
            exit("Извините, введённый вами логин уже зарегистрирован. Введите другой логин.");

        }
    }




} else{
    exit("<p><strong>Ошибка!</strong> Вы зашли на эту страницу напрямую, поэтому нет данных для обработки. Вы можете перейти на <a href=".$address_site."> главную страницу </a>.</p>");
}













