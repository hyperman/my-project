<?php
require_once __DIR__."/Autoload.php";
session_start();?>
<?php
if(empty($_SESSION['username'])) {
    header("Location: /form_auth.php");
    //header("location: /form_register.php");
    exit;
}
require_once __DIR__."/admin_reduct.php";
?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home</title>

    <link href="/public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/bootstrap/css/font-awesome.min.css" rel="stylesheet">
    <link href="/public/bootstrap/css/main.css" rel="stylesheet">



</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div>
        <a class="navbar-brand" href="#">Yevg <i class="glyphicon-eur"></i>niy</a>
    </div>
            <div class="container">
              <ul class="nav navbar-nav nav-pills navbar-right">
                 <li>
                    <a href="/home">Home</a>
                 </li>
                  <li>
                      <a href='/logout.php'>Sign out</a></a>
                  </li>
              </ul>
        </div>
</div>




  <form  action="admin_reduct.php" method="post" name="admin">

      <div class="container-fluid admin-panel">





           <div class="col-md-4">


                <h2 class="text-center h2-admin">Main Information</h2>
               <br>
                <p class="p-admin">Name (Ex:Sergey)</p>

                <input class="form-control" type="text" name="name"  placeholder="Name" />
                <p class="p-admin">Surname (Ex: Ivanov)</p>
                <input class="form-control" type="text" name="surname"  placeholder="Surname" />


                <div class="form-inline">
                    <p class="p-admin">Date of birth (D:M:Y)</p>

                    <select class="form-control" name="day">
                        <option></option>

                        <?php

                        for ($days=1; $days<=31; $days++  )
                        {
                            echo "<option value='{$days}'>{$days}</option>";
                        }
                        ?>
                    </select>
                    <select class="form-control" name="month">
                        <option></option>
                        <?php
                        foreach($months as $_months) {
                            echo "<option value='{$_months}'>{$_months}</option>";
                        }
                        ?>
                    </select>

                    <select class="form-control" name="year">
                        <option></option>
                        <?php
                        for($year=1960; $year<=1998; $year++)
                        {
                            echo "<option value='{$year}'>{$year}</option>";
                        }
                        ?>
                    </select>
                </div>

                   <p class="p-admin">Marital status</p>

                     <select class="form-control"  name="marital_status">
                         <option></option>
                          <?php
                          foreach($marital as $mary) {
                             echo "<option value='{$mary}'>{$mary}</option>";
                             }
                              ?>
                     </select>
           </div>


          <div class="col-md-4">
              <div class="form-group">

                <h2 class="text-center h2-admin">Education</h2>
                  <br>
                  <div class="form-inline">

                    <p class="p-admin">Period (Ex: 2008 - 2012)</p>
                    <select class="form-control" name="year_start">
                         <option></option>
                         <?php
                         for($start=1974; $start<=2020; $start++)
                        {
                        echo "<option value='{$start}'>{$start}</option>";
                         }
                         ?>
                     </select>

                     <select class="form-control" name="year_end">
                         <option></option>
                         <?php
                         for($end=1976; $end<=2020; $end++)
                        {
                         echo "<option value='{$end}'>{$end}</option>";
                         }
                        ?>
                    </select>
              </div>


              <p class="p-admin">Institution</p>
              <input class="form-control" type="text" name="institution"  placeholder="Institution" />

              <p class="p-admin">Specialty</p>
              <input class="form-control" type="text" name="specialty"  placeholder="Specialty" />

                <div class="form-inline">
                    <p class="p-admin">Period (Ex: 2008 - 2012)</p>

                    <select class="form-control" name="year_start_2" >
                        <option></option>
                         <?php
                         for($start=1974; $start<=2020; $start++)
                         {
                         echo "<option value='{$start}'>{$start}</option>";
                         }
                         ?>
                    </select>

                    <select class="form-control" name="year_end_2">
                    <option></option>
                     <?php
                     for($end=1976; $end<=2020; $end++)
                     {
                       echo "<option value='{$end}'>{$end}</option>";
                    }
                     ?>
                    </select>
                </div>

                <p class="p-admin">Institution</p>
                <input class="form-control" type="text" name="institution_2"  placeholder="Institution" />

                <p class="p-admin">Specialty</p>
                <input class="form-control" type="text" name="specialty_2"  placeholder="Specialty" />
            </div>
        </div>



        <div class="col-md-4">
            <div class="form-group">

                 <h2 class="text-center h2-admin">Additional information</h2>
                 <br>
                 <p class="p-admin">Languages</p>
                <input class="form-control" type="text" name="languages"  placeholder="Languages" />

                <p class="p-admin">Programming languages</p>
                 <input class="form-control" type="text" name="programming_languages"  placeholder="Programming languages" />

                 <p class="p-admin">Subversions</p>
                 <input class="form-control" type="text" name="subversions"  placeholder="Subversions" />

                 <p class="p-admin">OS (Ex: Git)</p>

                <input class="form-control" type="text"  name="OS"  placeholder="OS" />
            </div>
        </div>
    </div>


        <div class="col-lg-12">
            <br><br>
            <button type="submit" class="btn btn-success btn-lg center-block" name="btn_submit_admin" value="Change">
                <i class="glyphicon glyphicon-ok"></i>
                Change Data</button>

        </div>

</form>
<div class="navbar-nav">

</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/public/bootstrap/js/bootstrap.min.js"></script>
</body>


<!-- /.container -->






