<?php
session_start();
require_once __DIR__. "/templates/form_header.php";
?>
    <div class="block_for_messages">
<?php
    if(isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])){
        echo $_SESSION["error_messages"];
    //Уничтожаем чтобы не выводились заново при обновлении страницы
       unset($_SESSION["error_messages"]);
}
//Если в сессии существуют радостные сообщения, то выводим их
    if(isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])){
        echo $_SESSION["success_messages"];

    //Уничтожаем чтобы не выводились заново при обновлении страницы
       unset($_SESSION["success_messages"]);
}
?>
    </div>
<?php
//Проверяем, если пользователь не авторизован, то выводим форму регистрации,
//иначе выводим сообщение о том, что он уже зарегистрирован
if(!isset($_SESSION["email"]) && !isset($_SESSION["password"])){

    ?>
<div class="content">
<div class="form-wrapper">
    <div class="linker">
        <span class="ring"></span>
    </div>
    <div id="form_register">

            <form class="login-form" action="register.php" method="post" name="form_register">
                <input type="text" name="first_name" required="required" placeholder="First_name" />
                <input type="text" name="last_name" required="required" placeholder="Last_name" />

                <input type="text" name="username" required="required" placeholder="User_name" />

                <input type="password" name="password" required="required" placeholder="Password" />
                <span id="valid_password_message" class="mesage_error"></span>
                <button type="submit" name="btn_submit_register"
                        value="Зарегистрироватся!">Registration</button>

            </form>
    </div>
        </div>
</div>

    <?php
    }else{

    ?>
    <div id="authorized">
        <h2>Вы уже зарегистрированы</h2>
    </div>
    <?php
}
?>

