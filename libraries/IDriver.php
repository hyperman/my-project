<?php

namespace libraries;

interface IDriver {

    public function __construct($server, $username, $password, $database);

    public function getConnection();

    public function pdo_query($field, $value, $table, $end_query);

    //public function select_all();

    public function __destruct();

    public function find($table, $id = null);
}
