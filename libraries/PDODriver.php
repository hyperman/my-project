<?php

namespace libraries;

class PDODriver implements IDriver
{

    private $connection;

    public function __construct($server, $username, $password, $database)
    {
        $this->connection = new \PDO("mysql:host={$server};dbname={$database}", $username, $password);


    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function pdo_query($field, $value, $table, $end_query){
        $field_query = "UPDATE $table SET $field = '$value' $end_query";
        $pdo_connect = $this->getConnection();

        $query_name = $pdo_connect->prepare($field_query);
        $query_name->execute();
    }





    public function __destruct()
    {

    }

    public function find($table, $id = null)
    {
        if (!empty($id)) {
            $sql = "SELECT * FROM {$table} WHERE id =:id";
            $stm = $this->connection->prepare($sql);
            $parameters = ['id' => $id];
            $execute_res = $stm->execute($parameters);
            if ($execute_res == false) {
                throw new \Exception($stm->errorInfo());
            }
            return $stm->fetchObject();
        } else {
            $sql = $sql = "SELECT * FROM {$table}";
            $stm = $this->connection->query($sql);
            if ($stm == false) {
                throw new \Exception($this->connection->errorInfo());
            }
            return $stm->fetchAll(\PDO::FETCH_OBJ);
        }

    }

    public function delete($table, $id = null)
    {
        if (!empty($id)) {
            $sql = "DELETE FROM {$table} WHERE id =:id";
            $stm = $this->connection->prepare($sql);
            $parameters = ['id' => $id];
            $execute_res = $stm->execute($parameters);
            if ($execute_res == false) {
                throw new \Exception($stm->errorInfo()[2]);
            }

            else {
                $sql = $sql = "DELETE FROM {$table}";
                $stm = $this->connection->prepare($sql);

                $execute_res = $stm->execute($parameters);

            }

        }


    }
///////////////////////////////////////////////////////////////////////////

    public function insert($table, array $properties){

        $sql = "INSERT INTO `{$table}` ";

        $sql .= "(" . implode(',', array_keys($properties)) .")";

        $sql .= " VALUES ";

        foreach($properties as $column_name => $value){
            $values_array[] = ":{$column_name}";

        }
        $sql .= "(" . implode(',', $values_array) .")";

        $stm = $this->connection->prepare($sql);
        $execute_res = $stm->execute($properties);
        if($execute_res === false) {
            throw new \Exception($stm->errorInfo()[2]);
        }
        return $this->connection->lastInsertId();




    }
///////////////////////////////////////////////////////////////////

    public function update($table, $id, array $properties){
        $sql = "UPDATE {$table} SET ";

        $update_sql = [];
        foreach($properties as $column_name => $value) {
            $update_sql[] = "{$column_name} = :{$column_name}";
        }

            $sql.= implode(",", $update_sql);
            $sql.= " WHERE id = :id";
            $stm = $this->connection->prepare($sql);
            $properties['id'] = $id;

            $execute_res = $stm->execute($properties);
            if($execute_res == false) {
                throw new \Exception($stm->errorInfo()[2]);
            }





    }

}