<?php

require_once __DIR__. "/Autoload.php";

session_start();

$user = new \auth\Auth();
$user->logout();
header("Location: /index.php");

