<?php

namespace classes;

require_once __DIR__ . "/../Autoload.php";

class Page {

    protected $title;
    protected $template;
    public $template_data;


    public function getTemplate() {       //***  метод возвращает содержимое переменной $template
        return $this->template;        // в нашем случае возвращаем  $this->template = 'home.html' из файла Home.php
                                          //njtcnm  выводиться на экран файл Home.html (ДОМАШНЯЯ СТРАНИЦА)
    }

    public function getTemplateData() {       //***  метод возвращает содержимое переменной $template
        return $this->template_data;        // в нашем случае возвращаем  $this->template = 'home.html' из файла Home.php
                                          //njtcnm  выводиться на экран файл Home.html (ДОМАШНЯЯ СТРАНИЦА)
    }

    public function getTitle() {           /*метод возвращает содержимое переменной $title,
            а титл равен $this->title = 'templates.html';
           ТОЕСТЬ ВЫВОДИТ templates.html*/

        return $this->title;
    }



}
