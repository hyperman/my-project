<?php
class Autoload {

    public static function loadClass($className) {
        $classPath = str_replace('\\', '/', $className);
        $classPath = __DIR__ . '/' . $classPath . '.php';

        if(file_exists($classPath)) {
            require_once $classPath;
        } else {
            throw new Exception('НЕ НАЙДЕН ФАЙЛ!');
        }
    }

}
spl_autoload_register(["Autoload", 'loadClass']);