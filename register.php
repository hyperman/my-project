<?php
require_once __DIR__."/Autoload.php";
session_start();
require_once __DIR__. "/templates/form_header.php";
//Добавляем файл подключения к БД
require_once __DIR__."/dbconnect.php";
//Объявляем ячейку для добавления ошибок, которые могут возникнуть при обработке формы.
$_SESSION["error_messages"] = '';

//Объявляем ячейку для добавления успешных сообщений
$_SESSION["success_messages"] = '';

if(isset($_POST["btn_submit_register"]) && !empty($_POST["btn_submit_register"])){

    if(isset($_POST["first_name"])){

        //Обрезаем пробелы с начала и с конца строки
        $first_name = trim($_POST["first_name"]);

        //Проверяем переменную на пустоту
        if(!empty($first_name)){
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $first_name = htmlspecialchars($first_name, ENT_QUOTES);
        }else{
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваше имя</p>";

            //Возвращаем пользователя на страницу регистрации

            //Останавливаем скрипт
            exit();
        }


    }else{
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле с именем</p>";

        //Возвращаем пользователя на страницу регистрации
        // header("HTTP/1.1 301 Moved Permanently");
        //header("Location: ".$address_site."/form_register.php");
        //Останавливаем скрипт
        exit();
    }


    if(isset($_POST["last_name"])){

        //Обрезаем пробелы с начала и с конца строки
        $last_name = trim($_POST["last_name"]);

        if(!empty($last_name)){
            // Для безопасности, преобразуем специальные символы в HTML-сущности
            $last_name = htmlspecialchars($last_name, ENT_QUOTES);
        }else{

            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Вашу фамилию</p>";

            //Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".$address_site."/form_register.php");

            //Останавливаем  скрипт
            exit();
        }

    }else{

        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле с фамилией</p>";

        //Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$address_site."/form_register.php");

        //Останавливаем  скрипт
        exit();
    }

    if(isset($_POST["username"])){
        //Обрезаем пробелы с начала и с конца строки
        $username = trim($_POST["username"]);

        $query_test = "SELECT id FROM users WHERE username = '$username'";
        $test_user = $pdo_driver->getConnection()->query($query_test);
        $batva = $test_user->fetch(PDO::FETCH_ASSOC);

        //Проверяем переменную на пустоту
        if(!empty($username)){
            header("Location: ".$address_site."/intopage.php");
            $username = htmlspecialchars($username, ENT_QUOTES);

            if(isset($batva['id'])){
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$address_site."/form_register.php");
               // $header_into = file_get_contents("intopage.php");
               // echo $header_into;
                exit("Извините, введённый вами логин уже зарегистрирован. Введите другой логин.
                <a href='/form_register.php'>Registration</a>");
            } else {
                header("Location: ".$address_site."/intopage.php");
            }


            // Для безопасности, преобразуем специальные символы в HTML-сущности

        }else{
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваш Login</p>";

            //Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".$address_site."/form_register.php");
            //Останавливаем скрипт
            exit();
        }


    }else{
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле с именем</p>";

        //Возвращаем пользователя на страницу регистрации


        //Останавливаем скрипт
        exit();
    }

    if(isset($_POST["password"])){

        //Обрезаем пробелы с начала и с конца строки
        $password = trim($_POST["password"]);

        if(!empty($password)){
            $password = htmlspecialchars($password, ENT_QUOTES);

            //Шифруем папроль
            $password = md5($password."top_secret");
        }else{
            // Сохраняем в сессию сообщение об ошибке.
            $_SESSION["error_messages"] .= "<p class='mesage_error'>Укажите Ваш пароль</p>";

            //Возвращаем пользователя на страницу регистрации

            //Останавливаем  скрипт
            exit();
        }

    }else{
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error'>Отсутствует поле для ввода пароля</p>";
        //Возвращаем пользователя на страницу регистрации
        //header("HTTP/1.1 301 Moved Permanently");
        //header("Location: ".$address_site."/form_register.php");

        //Останавливаем  скрипт
        exit();
    }

    //проверка на существование пользователя с таким же логином



    $insert_query = "INSERT INTO users (firstname, lastname, username, password) VALUES ('".$first_name."', '".$last_name."', '".$username."', '".$password."')";
    $result_query_insert = $pdo_driver->getConnection()->prepare($insert_query);

    $result_query_insert->execute();

    //Запрос на добавления пользователя в БД
    //$result_query_insert = $mysqli->query("INSERT INTO `users` (first_name, last_name, username, password) VALUES ('".$first_name."', '".$last_name."', '".$email."', '".$password."')");

    if(!$result_query_insert){
        // Сохраняем в сессию сообщение об ошибке.
        $_SESSION["error_messages"] .= "<p class='mesage_error' >Ошибка запроса на добавления пользователя в БД</p>";

        //Возвращаем пользователя на страницу регистрации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$address_site."/form_register.php");
        //Останавливаем  скрипт
        exit();
    }else{

        $_SESSION["success_messages"] = "<p class='success_message'>Регистрация прошла успешно!!! <br />Теперь Вы можете авторизоваться используя Ваш логин и пароль.</p>";
        header("Location: /admin.php");
        echo "Вы успешно зарегистрированы! Теперь вы можете зайти на сайт. <a href='/login.php'>Sign In</a>";
        //Отправляем пользователя на страницу авторизации
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$address_site."/form_auth.php");

    }

    /* Завершение запроса */
    $result_query_insert = null;
    $pdo_driver = null;

//Закрываем подключение к Б
} else{
    exit("<p><strong>Ошибка!</strong> Вы зашли на эту страницу напрямую, поэтому нет данных для обработки. Вы можете перейти на <a href=".$address_site."> главную страницу </a>.</p>");
}